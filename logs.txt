Request method:	POST
Request URI:	https://restful-booker.herokuapp.com/booking
Proxy:			<none>
Request params:	<none>
Query params:	<none>
Form params:	<none>
Path params:	<none>
Headers:		Accept=*/*
				Content-Type=application/json
Cookies:		<none>
Multiparts:		<none>
Body:
{
    "firstname": "fnvalue1",
    "lastname": "lnvalue1",
    "totalprice": 100,
    "depositpaid": true,
    "additionalneeds": "Breakfast",
    "bookingdates": {
        "checkin": "2018-01-01",
        "checkout": "2018-01-02"
    }
}
HTTP/1.1 200 OK
Server: Cowboy
Connection: keep-alive
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 205
Etag: W/"cd-KS2dUJ4V6cieMv0lxGVHhFxFBDE"
Date: Sun, 02 Oct 2022 01:44:03 GMT
Via: 1.1 vegur

{
    "bookingid": 3806,
    "booking": {
        "firstname": "fnvalue1",
        "lastname": "lnvalue1",
        "totalprice": 100,
        "depositpaid": true,
        "bookingdates": {
            "checkin": "2018-01-01",
            "checkout": "2018-01-02"
        },
        "additionalneeds": "Breakfast"
    }
}
